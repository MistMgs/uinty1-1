fg

// 로딩
showloading
wait 0.1

loadbg horse
loadbg Town
loadbg Room
loadmodel Haru
loadmodel Epsilon

bgm Whisper
wait 0.1
removeloading
showmenu


text Lecture No.1\n- 게임을 만들어봅시다.

bg Town
fgout 1.0
wait 0.5

model Epsilon angry true empty (0,-1,0) 1.0


motion Epsilon angry
expression Epsilon

name 학생
text 선생 어디갔어! 이런곳에서 뭘 만든다는거야!
motion Epsilon angry

model Haru idle true empty (1,-1,0) 1.0
name 선생
text 장소가 마음에 들지 않으신가요?
text 그럼 장소를 옮기도록 하죠.
motion Haru idel

modelhide Epsilon
modelhide Haru


bg horse
fgout 1.0
wait 0.5

model Haru idle true empty (1,-1,0) 1.0
name 선생
text 이 장소는 어떤가요? 
text 대자연의 기운을 느낄 수 있습니다.
motion Haru idel

model Epsilon happy true empty (-1,-1,0) 1.0
name 학생
text 아주 훌륭한 장소 입니다. 선생님.
text 멋진 게임을 만들 수 있을 것 같아요.
motion Epsilon happy

expression Haru smile
name 선생
text 잘 됐네요. 
text 그럼 게임을 만들기 앞서 기본적인 방식을 알아보죠.
text 기획하기, 프로토타입 제작,리소스 제작, 코드 짜기, 버그 양산하기
text 끝도 없네요.
motion Haru easy

name 학생
text 걱정마세요.
text 열심히 하면 뭐든지 가능해요.
motion Epsilon happy

name 선생
text ....... (안될걸)
text 자 그럼 당신이 해야할 것을 말해 주겠습니다.
text 1.기획하기 \n2. 리소스제작 \n3. 코딩 \n4. 폴리싱...
motion Haru idle

label selectStart
expression Epsilon empty
motion Epsilon idle true
name
text 자 그럼 어떤 게임을 만들고 싶나요?

select
selectitem RPG RPG 게임
selectitem mi 미연시 게임
selectitem practice 숨쉬기
selectend

label RPG
name 학생
text RPG 게임을 만들고 싶어요.
text 막 액션감 쩔고!

motion Epsilon smile true 
expression Epsilon empty

name 선생
text 포기하세요. 
motion Haru smile true 
motion Epsilon sad true

text .......
text 그럼 이제 게임을 만들어 볼까요.

jump selectStart

label mi
name 학생
text 미연시 게임이라면 만들 수 있어요!
motion Haru smile true 
motion Epsilon sad true

name 선생
text 외주할 돈만 많다면 불가능하진 않겠네요.
text 돈 많으세요?
expression Epsilon sad


text .......
text 그럼 이제 게임을 만들어 볼까요.

jump selectStart

label practice

name 학생
text 숨쉬는 게임이라면 만들 수 있어요!
motion Haru idel true 
motion Epsilon sad true

name 선생
text 현명하군요.
text 하지만, 숨쉬는 것도 쉽진 않습니다.
text 당신의 빨아들이는 산소의 양과 내 뱉을 때\n나오는 이산화 탄소의 질량을 계산할 수 있으세요?
text 음, 그래도 뭐 시작해 볼까요.
expression Haru idle
expression Epsilon sad

modelhide Epsilon
modelhide Haru

fg 1.0
wait 1.0

hidetext

bg Room
fgout 1.0
wait 0.5

model Epsilon sad true empty (0,-1,0) 1.0


motion Epsilon sad
expression Epsilon

name 학생
text 으으... 숨쉬는게 이렇게 어려울 줄이야...
motion Epsilon sad


text 그래도 포기할 수 없어. 꼭 만들겠어

loadmodel guan
model guan idle true empty (0,-1,0) 1.0
